/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

in vec3 vertexNormalRespectToCamera; //нормаль в системе координат камеры
in vec4 vertexPositionRespectToCamera; //координаты вершины в системе координат камер
in vec4 color;

out vec4 fragColor;

void main() {
    if(dot(vertexNormalRespectToCamera, vertexPositionRespectToCamera.xyz) < 0) {
        fragColor.rgb = vertexNormalRespectToCamera.xyz * (-0.5) + 0.5;
    } else {
        fragColor.rgb = vertexNormalRespectToCamera.xyz * 0.5 + 0.5;
    }
    fragColor.a = 1.0;
}
